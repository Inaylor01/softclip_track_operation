This App generates a coverage graph from an alignment track, using only the soft-clip features.

To run the the App:

1. Install the App.
2. Right-click an alignment track (such as from a loaded BAM file)
3. Select **Added App - Graph Soft Clips**
 
Observe that a new graph appears named for the selected alignments track.
