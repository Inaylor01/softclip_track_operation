package org.lorainelab.igb.trackoperation;

// for declarative services
import aQute.bnd.annotation.component.Component;

// IGB platform classes
import com.affymetrix.genometry.BioSeq;
import com.affymetrix.genometry.operator.Operator;
import com.affymetrix.genometry.parsers.FileTypeCategory;
import com.affymetrix.genometry.symmetry.impl.SeqSymmetry;

// java
import java.util.List;

// in this repo
import org.lorainelab.igb.trackoperation.util.SoftClipUtils;

/**
 *
 * @author noorzahara
 */
@Component(immediate = true)
public class SoftClipDepthOperator implements Operator {

    protected final FileTypeCategory fileTypeCategory;

    public SoftClipDepthOperator() {
        fileTypeCategory = FileTypeCategory.Alignment;
    }

    @Override
    public String getName() {
        return fileTypeCategory.toString().toLowerCase() + "_softclip_depth";
    }

    @Override
    public String getDisplay() {
        return "Added App - Graph Soft-Clips";
    }

    @Override
    public SeqSymmetry operate(BioSeq aseq, List < SeqSymmetry > symList) {
        return SoftClipUtils.getSymmetrySoftclipSummary(symList, aseq, false, null);
    }

    @Override
    public FileTypeCategory getOutputCategory() {
        return FileTypeCategory.Graph;
    }

    @Override
    public int getOperandCountMin(FileTypeCategory category) {
        return category == fileTypeCategory ? 1 : 0;
    }

    @Override
    public int getOperandCountMax(FileTypeCategory category) {
        return category == fileTypeCategory ? 1 : 0;
    }

    @Override
    public boolean supportsTwoTrack() {
        return false;
    }

    @Override
    public Operator newInstance() {
        try {
            return getClass().getConstructor().newInstance();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
    @Override
    public int getOrder() {
      return 1;
    }  
    */

}